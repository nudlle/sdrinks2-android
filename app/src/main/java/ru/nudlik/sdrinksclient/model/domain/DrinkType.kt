package ru.nudlik.sdrinksclient.model.domain

enum class DrinkType {
    Lemonade, Tea, Coffee, Shot, Cocktail, Milkshake, Liqueur, Unspecified
}
