package ru.nudlik.sdrinksclient.model.http.rest

import io.reactivex.disposables.CompositeDisposable
import okhttp3.MultipartBody
import ru.nudlik.sdrinksclient.model.domain.Subscription
import ru.nudlik.sdrinksclient.model.domain.UserMessage
import ru.nudlik.sdrinksclient.model.http.RestClient
import ru.nudlik.sdrinksclient.security.UserRequest
import javax.inject.Inject

class UserRestImpl constructor(val restClient: RestClient, val responseHandler: ResponseHandler)
    : UserRest {
    private val compositeDisposable = CompositeDisposable()
    override fun registerUser(email: String, emailHash: String, user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.registerImpl(
                restClient,
                email,
                emailHash,
                user
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun loginUser(user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.loginImpl(restClient, user)
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun logoutUser(userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.logoutImpl(restClient, userId)
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun loginWithPassword(user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.loginByPasswordImpl(
                restClient,
                user
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun checkEmailOnRegisterRequest(email: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.checkEmailImpl(restClient, email)
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun checkEmailOnRecoverRequest(email: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.recoverEmailHashRequestImpl(
                restClient,
                email
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun recoverWithEmail(emailHash: String, user: UserRequest, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.recoverByEmailImpl(
                restClient,
                emailHash,
                user
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun postUserAvatar(userId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadUserAvatarImpl(
                restClient,
                userId,
                file
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun addLikeRecipe(userId: String, recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.addLikedRecipeImpl(
                restClient,
                userId,
                recipeId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun deleteLikeRecipe(userId: String, recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.deleteLikedRecipeImpl(
                restClient,
                userId,
                recipeId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun postAppError(userMessage: UserMessage, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.postAppErrorMessageImpl(
                restClient,
                userMessage
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun getUser(userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getUser(restClient, userId)
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun createSub(subscription: Subscription, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.createSubImpl(
                restClient,
                subscription
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}