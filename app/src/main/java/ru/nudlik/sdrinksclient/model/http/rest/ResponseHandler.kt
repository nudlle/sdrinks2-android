package ru.nudlik.sdrinksclient.model.http.rest

import android.app.Application
import android.content.DialogInterface
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.model.domain.Recipe
import ru.nudlik.sdrinksclient.model.http.ErrorHandler
import ru.nudlik.sdrinksclient.model.http.response.DataResponse
import ru.nudlik.sdrinksclient.model.http.response.ServerResponseCode

class ResponseHandler constructor(private val application: Application) {
    var showExceptionDialog = false
    fun <T> consume(callback: ResponseCallback) = { response: DataResponse<T>?, e: Throwable? ->
        if (response != null) {
            if (response.code == ServerResponseCode.OK_RESPONSE) {
                callback.success(response.data)
            } else {
                callback.errorCode(response.code)
            }
        } else {
            if (showExceptionDialog) {
                ErrorHandler.processErrorImpl(
                    application,
                    e,
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            }
            callback.exception(e)
        }
    }

    fun <T> consumeList(callback: ResponseCallback) = { list: List<DataResponse<T>>?, e: Throwable? ->
        if (list != null) {
            val data = ArrayList<T>(list.size)
            list.filter { it.code == ServerResponseCode.OK_RESPONSE }
                .map { it.data }
                .forEach { it?.let { nonNull -> data.add(nonNull) } }
            callback.success(data)
        } else {
            if (showExceptionDialog) {
                ErrorHandler.processErrorImpl(
                    application,
                    e,
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            }
            callback.exception(e)
        }
    }

    fun <T> convert(list: List<DataResponse<T>>?): List<T> {
        if (list != null) {
            return list.filter { it.code == ServerResponseCode.OK_RESPONSE }
                .filter { it.data != null }
                .map { it.data!! }
        }
        return emptyList()
    }

    fun consumeCode(callback: ResponseCallback) = { responseCode: ServerResponseCode?, e: Throwable? ->
        if (responseCode != null) {
            if (responseCode == ServerResponseCode.OK_RESPONSE) {
                callback.success(responseCode)
            } else {
                callback.errorCode(responseCode)
            }
        } else {
            if (showExceptionDialog) {
                ErrorHandler.processErrorImpl(
                    application,
                    e,
                    DialogInterface.OnClickListener { dialog, _ ->
                        dialog.dismiss()
                    })
            }
            callback.exception(e)
        }
    }

    fun getStringResponseRes(responseCode: ServerResponseCode?): Int {
        return when (responseCode) {
            ServerResponseCode.UNIQUE_EMAIL_EXCEPTION -> {
                R.string.email_unique_validation_title
            }
            ServerResponseCode.COMMENT_NOT_FOUND -> {
                R.string.comment_not_found_title
            }
            ServerResponseCode.VALIDATION_EXCEPTION -> {
                R.string.validation_exception_title
            }
            ServerResponseCode.USER_NOT_FOUND -> {
                R.string.user_not_found_title
            }
            ServerResponseCode.LOGIN_FAILED -> {
                R.string.login_failed_title
            }
            ServerResponseCode.FILE_IS_TOO_BIG -> {
                R.string.invalid_file_size_title
            }
            ServerResponseCode.FILE_UPLOAD_FAILED -> {
                R.string.upload_failed_title
            }
            ServerResponseCode.IMAGE_RESIZE_FAILED -> {
                R.string.image_resize_failed_title
            }
            ServerResponseCode.RECIPE_NOT_FOUND -> {
                R.string.recipe_not_found_title
            }
            ServerResponseCode.RESOURCE_NOT_FOUND -> {
                R.string.resource_not_found_title
            }
            ServerResponseCode.UNKNOWN_EXCEPTION -> {
                R.string.unknown_exception_title
            }
            ServerResponseCode.CHECK_EMAIL_FAILED -> {
                R.string.check_email_failed_title
            }
            ServerResponseCode.USER_MESSAGE_NOT_FOUND -> {
                R.string.app_error_not_found
            }
            ServerResponseCode.SUBSCRIPTION_NOT_FOUND -> {
                R.string.app_error_subscription_not_found
            }
            ServerResponseCode.SUBSCRIPTION_NOT_VALID -> {
                R.string.app_error_subscription_not_valid
            }
            else -> {
                R.string.unknown_exception_title
            }
        }
    }
}