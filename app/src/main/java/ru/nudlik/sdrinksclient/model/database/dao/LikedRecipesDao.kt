package ru.nudlik.sdrinksclient.model.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Flowable
import ru.nudlik.sdrinksclient.model.inner.domain.LikedRecipes

@Dao
interface LikedRecipesDao {
    @Query("SELECT * FROM likedrecipes")
    fun getAllLikedRecipes(): LiveData<List<LikedRecipes>>
    @Query("SELECT * FROM likedrecipes")
    fun getAllLikedRecipesList(): List<LikedRecipes>
    @Insert
    fun putLikedRecipe(lr: LikedRecipes)
    @Update
    fun updateLikedRecipe(lr: LikedRecipes)
    @Delete
    fun deleteLikedRecipe(lr: LikedRecipes)
}