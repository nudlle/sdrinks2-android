package ru.nudlik.sdrinksclient.model.domain

enum class DrinkTaste {
    Berries, Fruit, Vegetable, Tasty, Sour, Spicy, Herb, Bitter
}
