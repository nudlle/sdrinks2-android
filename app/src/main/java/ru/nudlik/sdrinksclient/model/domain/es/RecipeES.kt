package ru.nudlik.domain.es

import ru.nudlik.sdrinksclient.model.domain.*
import java.util.*

class RecipeES {
    var recipeESId: Long? = null
    var recipeId: String? = null
    var userId: String? = null
    var userEmail: String? = null
    var name: String? = null
    var secondaryName: String? = null
    var imageUrls: ImageUrls? = null
    var drinkState: DrinkState? = null
    var drinkType: DrinkType? = null
    var drinkTastes: List<DrinkTaste>? = null
    var drinkAlcohol: DrinkAlcohol? = null
    var hashTags: String? = null
    var commentCount: Long = 0
    var rate = 0.0
    var calcRate = 0.0
    var date: Date? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val recipeES = other as RecipeES
        return commentCount == recipeES.commentCount && java.lang.Double.compare(
            recipeES.rate,
            rate
        ) == 0 && java.lang.Double.compare(recipeES.calcRate, calcRate) == 0 &&
                recipeESId == recipeES.recipeESId &&
                recipeId == recipeES.recipeId &&
                userId == recipeES.userId &&
                userEmail == recipeES.userEmail &&
                name == recipeES.name &&
                secondaryName == recipeES.secondaryName &&
                imageUrls == recipeES.imageUrls && drinkState === recipeES.drinkState && drinkType === recipeES.drinkType &&
                drinkTastes == recipeES.drinkTastes && drinkAlcohol === recipeES.drinkAlcohol &&
                hashTags == recipeES.hashTags &&
                date == recipeES.date
    }

    override fun hashCode(): Int {
        return Objects.hash(
            recipeESId,
            recipeId,
            userId,
            userEmail,
            name,
            secondaryName,
            imageUrls,
            drinkState,
            drinkType,
            drinkTastes,
            drinkAlcohol,
            hashTags,
            commentCount,
            rate,
            calcRate,
            date
        )
    }

    override fun toString(): String {
        return "RecipeES{" +
                "recipeESId=" + recipeESId +
                ", recipeId='" + recipeId + '\'' +
                ", userId='" + userId + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", name='" + name + '\'' +
                ", secondaryName='" + secondaryName + '\'' +
                ", imageUrls=" + imageUrls +
                ", drinkState=" + drinkState +
                ", drinkType=" + drinkType +
                ", drinkTastes=" + drinkTastes +
                ", drinkAlcohol=" + drinkAlcohol +
                ", hashTags='" + hashTags + '\'' +
                ", commentCount=" + commentCount +
                ", rate=" + rate +
                ", calcRate=" + calcRate +
                ", date=" + date +
                '}'
    }
}