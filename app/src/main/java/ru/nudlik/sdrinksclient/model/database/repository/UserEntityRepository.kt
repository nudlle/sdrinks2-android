package ru.nudlik.sdrinksclient.model.database.repository

import androidx.lifecycle.LiveData
import ru.nudlik.sdrinksclient.model.inner.domain.UserEntity

interface UserEntityRepository {
    fun getAllUserEntities(): LiveData<List<UserEntity>>
    fun getUserEntityById(id: Long): LiveData<UserEntity>
    fun getAllUserEntitiesList(): List<UserEntity>
    fun putUserEntity(userEntity: UserEntity)
    fun updateUserEntity(userEntity: UserEntity)
    fun deleteUserEntity(userEntity: UserEntity)
}