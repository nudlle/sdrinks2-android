package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.model.domain.Subscription

class SubscriptionResponse : DataResponse<Subscription> {
    constructor()

    constructor(data: Subscription, code: ServerResponseCode) : super(data, code)

    constructor(data: Subscription) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
