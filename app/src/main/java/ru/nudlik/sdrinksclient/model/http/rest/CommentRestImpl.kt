package ru.nudlik.sdrinksclient.model.http.rest

import io.reactivex.disposables.CompositeDisposable
import ru.nudlik.sdrinksclient.model.domain.UserComment
import ru.nudlik.sdrinksclient.model.http.RestClient
import javax.inject.Inject

class CommentRestImpl @Inject constructor(val restClient: RestClient, val responseHandler: ResponseHandler)
    : CommentRest {
    private val compositeDisposable = CompositeDisposable()

    override fun postComment(userComment: UserComment, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.createCommentImpl(
                restClient,
                userComment
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun getCommentsByRecipeId(recipeId: String, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getCommentsOfRecipeImpl(
                restClient,
                recipeId,
                page,
                size
            )
                ?.subscribe(responseHandler.consumeList(callback))
        )
    }

    override fun isCommentPosted(recipeId: String, userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.isCommentAlreadyPostedImpl(
                restClient,
                recipeId,
                userId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}