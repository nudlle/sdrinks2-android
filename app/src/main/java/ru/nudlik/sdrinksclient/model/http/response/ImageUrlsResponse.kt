package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.model.domain.ImageUrls

class ImageUrlsResponse : DataResponse<ImageUrls> {
    constructor()

    constructor(data: ImageUrls, code: ServerResponseCode) : super(data, code)

    constructor(data: ImageUrls) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
