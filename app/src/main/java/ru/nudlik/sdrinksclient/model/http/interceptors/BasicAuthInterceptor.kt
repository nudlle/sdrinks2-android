package ru.nudlik.sdrinksclient.model.http.interceptors

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response
import ru.nudlik.sdrinksclient.security.SessionToken

class BasicAuthInterceptor : Interceptor {
    private var credentials: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return if (credentials != null) {
            val authRequest = request.newBuilder().header("Authorization", credentials!!).build()
            chain.proceed(authRequest)
        } else {
            chain.proceed(request)
        }
    }

    fun addCredentials(token: SessionToken) {
        credentials = Credentials.basic(token.id!!, token.token!!)
    }

    fun deleteCredintials() {
        credentials = null
    }
}