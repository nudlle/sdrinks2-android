package ru.nudlik.sdrinksclient.model.http.rest

import okhttp3.MultipartBody
import ru.nudlik.sdrinksclient.model.domain.Subscription
import ru.nudlik.sdrinksclient.model.domain.UserMessage
import ru.nudlik.sdrinksclient.security.UserRequest

interface UserRest {
    fun registerUser(email: String, emailHash: String, user: UserRequest, callback: ResponseCallback)
    fun loginUser(user: UserRequest, callback: ResponseCallback)
    fun logoutUser(userId: String, callback: ResponseCallback)
    fun loginWithPassword(user: UserRequest, callback: ResponseCallback)
    fun checkEmailOnRegisterRequest(email: String, callback: ResponseCallback)
    fun checkEmailOnRecoverRequest(email: String, callback: ResponseCallback)
    fun recoverWithEmail(emailHash: String, user: UserRequest, callback: ResponseCallback)
    fun postUserAvatar(userId: String, file: MultipartBody.Part, callback: ResponseCallback)
    fun addLikeRecipe(userId: String, recipeId: String, callback: ResponseCallback = object : ResponseCallback {})
    fun deleteLikeRecipe(userId: String, recipeId: String, callback: ResponseCallback)
    fun postAppError(userMessage: UserMessage, callback: ResponseCallback)
    fun createSub(subscription: Subscription, callback: ResponseCallback)
    fun getUser(userId: String, callback: ResponseCallback)
    fun destroy()
}