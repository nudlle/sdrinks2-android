package ru.nudlik.sdrinksclient.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.nudlik.sdrinksclient.model.inner.domain.LikedRecipes
import ru.nudlik.sdrinksclient.model.inner.domain.UserEntity
import ru.nudlik.sdrinksclient.model.database.dao.LikedRecipesDao
import ru.nudlik.sdrinksclient.model.database.dao.UserEntityDao

@Database(entities = [UserEntity::class, LikedRecipes::class], version = 1)
@TypeConverters(TypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserEntityDao
    abstract fun likedRecipesDao(): LikedRecipesDao
}