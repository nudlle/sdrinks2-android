package ru.nudlik.sdrinksclient.model.http.rest

import ru.nudlik.sdrinksclient.model.http.response.ServerResponseCode

interface ResponseCallback {
    fun <T> success(data: T?) {}
    fun errorCode(responseCode: ServerResponseCode?) {}
    fun exception(e: Throwable?) {}
}