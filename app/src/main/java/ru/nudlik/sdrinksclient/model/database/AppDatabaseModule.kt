package ru.nudlik.sdrinksclient.model.database

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ru.nudlik.sdrinksclient.model.database.dao.LikedRecipesDao
import ru.nudlik.sdrinksclient.model.database.dao.UserEntityDao
import ru.nudlik.sdrinksclient.model.database.repository.LikedRecipesRepository
import ru.nudlik.sdrinksclient.model.database.repository.LikedRecipesRepositoryImpl
import ru.nudlik.sdrinksclient.model.database.repository.UserEntityRepository
import ru.nudlik.sdrinksclient.model.database.repository.UserEntityRepositoryImpl
import javax.inject.Singleton

@Module
class AppDatabaseModule(application: Application) {
    private val database: AppDatabase = Room.databaseBuilder(application, AppDatabase::class.java, "database").build()

    @Provides
    @Singleton
    fun provideAppDatabase(): AppDatabase {
        return this.database
    }

    @Provides
    @Singleton
    fun provideUserEntityDao(database: AppDatabase): UserEntityDao {
        return database.userDao()
    }

    @Provides
    @Singleton
    fun provideUserEntityRepository(userEntityDao: UserEntityDao): UserEntityRepository {
        return UserEntityRepositoryImpl(userEntityDao)
    }

    @Provides
    @Singleton
    fun provideLikedRecipesDao(database: AppDatabase): LikedRecipesDao {
        return database.likedRecipesDao()
    }

    @Provides
    @Singleton
    fun provideLikedRecipesRepository(likedRecipesDao: LikedRecipesDao): LikedRecipesRepository {
        return LikedRecipesRepositoryImpl(likedRecipesDao)
    }
}