package ru.nudlik.sdrinksclient.model.inner.domain

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.nudlik.sdrinksclient.model.domain.User

@Entity
class UserEntity {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
    @Embedded
    var user: User? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserEntity

        if (id != other.id) return false
        if (user != other.user) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (user?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "UserEntity(id=$id, user=$user)"
    }
}