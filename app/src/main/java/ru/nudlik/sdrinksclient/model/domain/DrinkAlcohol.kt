package ru.nudlik.sdrinksclient.model.domain

enum class DrinkAlcohol {
    Alco, NonAlco, Unspecified
}