package ru.nudlik.sdrinksclient.model.domain

enum class DrinkState {
    Hot, Cold, Unspecified
}