package ru.nudlik.sdrinksclient.model.database

import androidx.room.TypeConverter
import java.util.*
import java.util.stream.Collectors

class TypeConverter {
    @TypeConverter
    fun fromList(list: List<String>): String {
        return list.stream().collect(Collectors.joining(","))
    }
    @TypeConverter
    fun toList(s: String): List<String> {
        return s.split(',')
    }
    @TypeConverter
    fun fromEDate(date: Date): Long {
        return date.time
    }
    @TypeConverter
    fun toDate(time: Long): Date {
        return Date(time)
    }
}