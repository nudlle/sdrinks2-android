package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.model.domain.UserComment

class CommentResponse : DataResponse<UserComment> {
    constructor()

    constructor(data: UserComment, code: ServerResponseCode) : super(data, code)

    constructor(data: UserComment) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
