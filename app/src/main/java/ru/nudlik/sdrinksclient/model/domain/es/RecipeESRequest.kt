package ru.nudlik.domain.es

import ru.nudlik.sdrinksclient.model.domain.DrinkAlcohol
import ru.nudlik.sdrinksclient.model.domain.DrinkState
import ru.nudlik.sdrinksclient.model.domain.DrinkTaste
import ru.nudlik.sdrinksclient.model.domain.DrinkType
import java.util.*

class RecipeESRequest {
    var drinkState: DrinkState? = null
    var drinkType: DrinkType? = null
    var drinkTastes: List<DrinkTaste>? = null
    var drinkAlcohol: DrinkAlcohol? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as RecipeESRequest
        return drinkAlcohol === that.drinkAlcohol && drinkState === that.drinkState && drinkType === that.drinkType &&
                drinkTastes == that.drinkTastes
    }

    override fun hashCode(): Int {
        return Objects.hash(drinkState, drinkType, drinkTastes, drinkAlcohol)
    }

    override fun toString(): String {
        return "RecipeESRequest{" +
                "drinkState=" + drinkState +
                ", drinkType=" + drinkType +
                ", drinkTastes=" + drinkTastes +
                ", alcohol=" + drinkAlcohol +
                '}'
    }
}