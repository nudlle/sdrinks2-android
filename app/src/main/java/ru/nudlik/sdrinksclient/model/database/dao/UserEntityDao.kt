package ru.nudlik.sdrinksclient.model.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Flowable
import ru.nudlik.sdrinksclient.model.inner.domain.UserEntity

@Dao
interface UserEntityDao {
    @Query("SELECT * FROM userentity")
    fun getAllUserEntities(): LiveData<List<UserEntity>>
    @Query("SELECT * FROM userentity")
    fun getAllUserEntitiesFlowable(): Flowable<List<UserEntity>>
    @Query("SELECT * FROM userentity WHERE id = :id")
    fun getUserEntityById(id: Long): LiveData<UserEntity>
    @Query("SELECT * FROM UserEntity")
    fun getAllUserEntitiesList(): List<UserEntity>
    @Insert
    fun putUserEntity(userEntity: UserEntity)
    @Update
    fun updateUserEntity(userEntity: UserEntity)
    @Delete
    fun deleteUserEntity(userEntity: UserEntity)
}