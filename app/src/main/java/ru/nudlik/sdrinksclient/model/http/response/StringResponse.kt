package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.model.http.response.DataResponse
import ru.nudlik.sdrinksclient.model.http.response.ServerResponseCode

class StringResponse : DataResponse<String> {
    constructor()

    constructor(data: String, code: ServerResponseCode) : super(data, code)

    constructor(data: String) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
