package ru.nudlik.sdrinksclient.model.http.rest

import ru.nudlik.sdrinksclient.model.domain.UserComment

interface CommentRest {
    fun postComment(userComment: UserComment, callback: ResponseCallback)
    fun getCommentsByRecipeId(recipeId: String, page: Int, size: Int, callback: ResponseCallback)
    fun isCommentPosted(recipeId: String, userId: String, callback: ResponseCallback)
    fun destroy()
}