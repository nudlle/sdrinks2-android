package ru.nudlik.sdrinksclient.model.http.module

import android.app.Application
import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.model.http.*
import ru.nudlik.sdrinksclient.model.http.interceptors.BasicAuthInterceptor
import ru.nudlik.sdrinksclient.model.http.interceptors.UserAgentInterceptor
import ru.nudlik.sdrinksclient.model.http.rest.*
import java.lang.reflect.Type
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

@Module
class RestUtilModule @Inject constructor(application: Application, vararg interceptors: Interceptor = emptyArray()) {
    private val restClient: RestClient
    private val responseHandler: ResponseHandler
    private val basicAuthInterceptor: BasicAuthInterceptor
    private val httpClient: OkHttpClient
    private fun getTrustManagerFactory(ctx: Context): TrustManagerFactory {
        val factory = CertificateFactory.getInstance("X.509")
        val ca = factory.generateCertificate(ctx.resources.openRawResource(R.raw.sdrinks))
        val keystore = KeyStore.getInstance(KeyStore.getDefaultType())
        keystore.load(null, null)
        keystore.setCertificateEntry("sdrinks", ca)
        val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        tmf.init(keystore)
        return tmf
    }
    private fun getSslConfig(tmf: TrustManagerFactory): SSLContext {
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, tmf.trustManagers, null)
        return sslContext
    }
    init {
        // init OkHttpClient
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val tmf = getTrustManagerFactory(application)
        val builder = OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(UserAgentInterceptor("mf1.0"))
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .sslSocketFactory(getSslConfig(tmf).socketFactory, tmf.trustManagers[0] as X509TrustManager)
            .hostnameVerifier { _, _ -> true }
        for (ppp in interceptors) {
            builder.addInterceptor(ppp)
        }
        basicAuthInterceptor = BasicAuthInterceptor()
        builder.addInterceptor(basicAuthInterceptor)
        httpClient = builder.build()
        // init Retrofit2
        val gson = GsonBuilder()
            .registerTypeAdapter(Date::class.java, JsonDateDeserializer()).create()
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient)
            .baseUrl(RestClient.URL)
            .build()
        restClient = retrofit.create(RestClient::class.java)
        responseHandler = ResponseHandler(application)

        // init Picasso
        val picassoBuilder = Picasso.Builder(application)
        val httpDownloader = OkHttp3Downloader(httpClient)
        picassoBuilder.downloader(httpDownloader)
        Picasso.setSingletonInstance(picassoBuilder.build())
    }

    private class JsonDateDeserializer : JsonDeserializer<Date> {
        override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Date {
            return Date(json?.asJsonPrimitive!!.asLong)
        }
    }

    @Provides
    @Singleton
    fun provideRestClient(): RestClient {
        return restClient
    }

    @Provides
    @Singleton
    fun provideResponseHandler(): ResponseHandler {
        return responseHandler
    }

    @Provides
    @Singleton
    fun provideRecipesRest(restClient: RestClient, responseHandler: ResponseHandler): RecipesRest {
        return RecipesRestImpl(restClient, responseHandler)
    }

    @Provides
    @Singleton
    fun provideUserRest(restClient: RestClient, responseHandler: ResponseHandler): UserRest {
        return UserRestImpl(restClient, responseHandler)
    }

    @Provides
    @Singleton
    fun provideCommentRest(restClient: RestClient, responseHandler: ResponseHandler): CommentRest {
        return CommentRestImpl(restClient, responseHandler)
    }
    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        return this.httpClient
    }

    @Provides
    @Singleton
    fun provideBasicAuthInterceptor(): BasicAuthInterceptor {
        return this.basicAuthInterceptor
    }

    @Provides
    @Singleton
    fun providePicasso(): Picasso {
        return Picasso.get()
    }
}