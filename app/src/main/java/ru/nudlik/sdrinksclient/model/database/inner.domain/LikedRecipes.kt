package ru.nudlik.sdrinksclient.model.inner.domain

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class LikedRecipes {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
    var recipeIds: MutableList<String> = ArrayList()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LikedRecipes

        if (id != other.id) return false
        if (recipeIds != other.recipeIds) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + recipeIds.hashCode()
        return result
    }

    override fun toString(): String {
        return "LikedRecipes(id=$id, recipeIds=$recipeIds)"
    }
}