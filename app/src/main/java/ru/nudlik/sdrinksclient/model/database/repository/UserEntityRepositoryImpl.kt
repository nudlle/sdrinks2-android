package ru.nudlik.sdrinksclient.model.database.repository

import androidx.lifecycle.LiveData
import ru.nudlik.sdrinksclient.model.database.dao.UserEntityDao
import ru.nudlik.sdrinksclient.model.inner.domain.UserEntity
import javax.inject.Inject

class UserEntityRepositoryImpl @Inject constructor(val userEntityDao: UserEntityDao) : UserEntityRepository {
    override fun getAllUserEntities(): LiveData<List<UserEntity>> {
        return userEntityDao.getAllUserEntities()
    }

    override fun getUserEntityById(id: Long): LiveData<UserEntity> {
        return userEntityDao.getUserEntityById(id)
    }

    override fun getAllUserEntitiesList(): List<UserEntity> {
        return userEntityDao.getAllUserEntitiesList()
    }

    override fun putUserEntity(userEntity: UserEntity) {
        userEntityDao.putUserEntity(userEntity)
    }

    override fun updateUserEntity(userEntity: UserEntity) {
        userEntityDao.updateUserEntity(userEntity)
    }

    override fun deleteUserEntity(userEntity: UserEntity) {
        userEntityDao.deleteUserEntity(userEntity)
    }

}