package ru.nudlik.sdrinksclient.model.http.rest

import okhttp3.MultipartBody
import ru.nudlik.sdrinksclient.model.domain.DrinkType
import ru.nudlik.sdrinksclient.model.domain.Recipe
import ru.nudlik.sdrinksclient.model.domain.RecipesToSearch

interface RecipesRest {
    fun getRecipes(page: Int, size: Int, callback: ResponseCallback)
    fun getRecipeById(recipeId: String, callback: ResponseCallback)
    fun getRecipesByType(recipeType: DrinkType, page: Int, size: Int, callback: ResponseCallback)
    fun getRecipesByEmail(email: String, page: Int, size: Int, callback: ResponseCallback)
    fun searchRecipes(search: RecipesToSearch, page: Int, size: Int, callback: ResponseCallback)
    fun postRecipe(recipe: Recipe, callback: ResponseCallback)
    fun activateRecipe(recipeId: String, userId: String, callback: ResponseCallback)
    fun incView(recipeId: String, callback: ResponseCallback)
    fun incClick(adId: String, callback: ResponseCallback)
    fun deleteRecipe(recipeId: String, userId: String, callback: ResponseCallback)
    fun postRecipeImage(recipeId: String, file: MultipartBody.Part, callback: ResponseCallback)
    fun postRecipeStepImage(recipeId: String, stepId: String, file: MultipartBody.Part, callback: ResponseCallback)
    fun postRecipeAdImage(recipeId: String, adId: String, file: MultipartBody.Part, callback: ResponseCallback)
    fun destroy()
}