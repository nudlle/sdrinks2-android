package ru.nudlik.sdrinksclient.model.database.repository

import androidx.lifecycle.LiveData
import ru.nudlik.sdrinksclient.model.database.dao.LikedRecipesDao
import ru.nudlik.sdrinksclient.model.inner.domain.LikedRecipes

class LikedRecipesRepositoryImpl constructor(private val likedRecipesDao: LikedRecipesDao) : LikedRecipesRepository {
    override fun getAllLikedRecipes(): LiveData<List<LikedRecipes>> {
        return this.likedRecipesDao.getAllLikedRecipes()
    }

    override fun getAllLikedRecipesList(): List<LikedRecipes> {
        return this.likedRecipesDao.getAllLikedRecipesList()
    }

    override fun putLikedRecipe(lr: LikedRecipes) {
        this.likedRecipesDao.putLikedRecipe(lr)
    }

    override fun updateLikedRecipe(lr: LikedRecipes) {
        this.likedRecipesDao.updateLikedRecipe(lr)
    }

    override fun deleteLikedRecipe(lr: LikedRecipes) {
        this.likedRecipesDao.deleteLikedRecipe(lr)
    }
}