package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.model.domain.Recipe

class RecipeResponse : DataResponse<Recipe> {
    constructor()

    constructor(data: Recipe, code: ServerResponseCode) : super(data, code)

    constructor(data: Recipe) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
