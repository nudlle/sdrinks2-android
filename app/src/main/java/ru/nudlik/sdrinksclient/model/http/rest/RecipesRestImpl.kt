package ru.nudlik.sdrinksclient.model.http.rest

import io.reactivex.disposables.CompositeDisposable
import okhttp3.MultipartBody
import ru.nudlik.sdrinksclient.model.domain.DrinkType
import ru.nudlik.sdrinksclient.model.domain.Recipe
import ru.nudlik.sdrinksclient.model.domain.RecipesToSearch
import ru.nudlik.sdrinksclient.model.http.RestClient
import javax.inject.Inject

class RecipesRestImpl @Inject constructor(val restClient: RestClient, val responseHandler: ResponseHandler) : RecipesRest {

    private val compositeDisposable = CompositeDisposable()
    var showExceptionDialog = true

    override fun getRecipes(page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipesImpl(
                restClient,
                page,
                size
            )
                ?.subscribe(responseHandler.consumeList(callback))
        )
    }

//    override fun getRecipesList(page: Int, size: Int): List<Recipe> {
//        return RestClient.getRecipesListImpl(restClient, page, size)
//    }

    override fun getRecipeById(recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipeByIdImpl(
                restClient,
                recipeId
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun getRecipesByType(recipeType: DrinkType, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipesByTypeImpl(
                restClient,
                recipeType,
                page,
                size
            )
                ?.subscribe(responseHandler.consumeList(callback))
        )
    }

    override fun getRecipesByEmail(email: String, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.getRecipesByUserEmailImpl(
                restClient,
                email,
                page,
                size
            )
                ?.subscribe(responseHandler.consumeList(callback))
        )
    }

    override fun searchRecipes(search: RecipesToSearch, page: Int, size: Int, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.searchRecipesImpl(
                restClient,
                search,
                page,
                size
            )
                ?.subscribe(responseHandler.consumeList(callback))
        )
    }

    override fun postRecipe(recipe: Recipe, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.createRecipeImpl(
                restClient,
                recipe
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun activateRecipe(recipeId: String, userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.activateRecipeImpl(
                restClient,
                recipeId,
                userId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun incView(recipeId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.incRecipeViewedImpl(
                restClient,
                recipeId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun incClick(adId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.incRecipeAdClickedImpl(
                restClient,
                adId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun deleteRecipe(recipeId: String, userId: String, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.deleteRecipeImpl(
                restClient,
                recipeId,
                userId
            )
                ?.subscribe(responseHandler.consumeCode(callback))
        )
    }

    override fun postRecipeImage(recipeId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadRecipeImageImpl(
                restClient,
                recipeId,
                file
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun postRecipeStepImage(recipeId: String, stepId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadRecipeStepImageImpl(
                restClient,
                recipeId,
                stepId,
                file
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun postRecipeAdImage(recipeId: String, adId: String, file: MultipartBody.Part, callback: ResponseCallback) {
        compositeDisposable.add(
            RestClient.uploadRecipeAdImageImpl(
                restClient,
                recipeId,
                adId,
                file
            )
                ?.subscribe(responseHandler.consume(callback))
        )
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}