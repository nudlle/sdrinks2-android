package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.security.SessionToken

class SessionTokenResponse : DataResponse<SessionToken> {
    constructor()

    constructor(data: SessionToken, code: ServerResponseCode) : super(data, code)

    constructor(data: SessionToken) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
