package ru.nudlik.sdrinksclient.model.http.response

import ru.nudlik.sdrinksclient.model.domain.UserMessage


class UserMessageResponse : DataResponse<UserMessage> {
    constructor()

    constructor(data: UserMessage, code: ServerResponseCode) : super(data, code)

    constructor(data: UserMessage) : super(data)

    constructor(code: ServerResponseCode) : super(code)
}
