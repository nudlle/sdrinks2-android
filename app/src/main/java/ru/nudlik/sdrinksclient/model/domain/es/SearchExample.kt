package ru.nudlik.domain.es

import java.util.*

class SearchExample {
    var exampleId: Long? = null
    var example: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as SearchExample
        return exampleId == that.exampleId &&
                example == that.example
    }

    override fun hashCode(): Int {
        return Objects.hash(exampleId, example)
    }

    override fun toString(): String {
        return "SearchRecipeExamplesNames{" +
                "recipeNamesId=" + exampleId +
                ", example=" + example +
                '}'
    }
}