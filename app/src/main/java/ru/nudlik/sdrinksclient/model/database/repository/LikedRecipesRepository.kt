package ru.nudlik.sdrinksclient.model.database.repository

import androidx.lifecycle.LiveData
import ru.nudlik.sdrinksclient.model.inner.domain.LikedRecipes

interface LikedRecipesRepository {
    fun getAllLikedRecipes(): LiveData<List<LikedRecipes>>
    fun getAllLikedRecipesList(): List<LikedRecipes>
    fun putLikedRecipe(lr: LikedRecipes)
    fun updateLikedRecipe(lr: LikedRecipes)
    fun deleteLikedRecipe(lr: LikedRecipes)
}