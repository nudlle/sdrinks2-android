package ru.nudlik.sdrinksclient.interfaces

import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.sdrinksclient.model.http.rest.ResponseCallback

interface BSListener<T> {
    fun add(t: T)
    fun edit(t: T)
    fun delete(t: T)
}

interface RequestNextPageListener {
    fun loadNextPage(page: Int, size: Int, callback: ResponseCallback)
}

interface LoadNextPageCallback {
    fun loadFinished()
}

interface RequestImage {
    fun requestImage(requestCode: Int, filename: String, viewHolder: RecyclerView.ViewHolder)
}

interface PostImage {
    fun postImage(path: String)
}

interface SwipeListItemControllerAction {
    fun onLeftClicked(position: Int)
    fun onRightClicked(position: Int)
}

interface DialogButtonsListener {
    fun positiveClick(ext: Any? = null)
    fun negativeClick(ext: Any? = null) {}
}