package ru.nudlik.sdrinksclient.security

interface CryptoUtil {
    fun encode(alias: String, input: String): String
    fun decode(alias: String, encoded: String): String
    fun deleteKey(alias: String)
    companion object Factory {
        const val PIN_ALIAS = "pin_key_alias"
    }
}