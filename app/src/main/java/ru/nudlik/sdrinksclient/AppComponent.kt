package ru.nudlik.sdrinksclient

import android.app.Application
import com.squareup.picasso.Picasso
import dagger.Component
import okhttp3.OkHttpClient
import ru.nudlik.sdrinksclient.model.database.AppDatabaseModule
import ru.nudlik.sdrinksclient.model.database.dao.LikedRecipesDao
import ru.nudlik.sdrinksclient.model.database.dao.UserEntityDao
import ru.nudlik.sdrinksclient.model.database.repository.LikedRecipesRepository
import ru.nudlik.sdrinksclient.model.database.repository.UserEntityRepository
import ru.nudlik.sdrinksclient.model.http.interceptors.BasicAuthInterceptor
import ru.nudlik.sdrinksclient.model.http.module.RestUtilModule
import ru.nudlik.sdrinksclient.model.http.rest.CommentRest
import ru.nudlik.sdrinksclient.model.http.rest.RecipesRest
import ru.nudlik.sdrinksclient.model.http.rest.ResponseHandler
import ru.nudlik.sdrinksclient.model.http.rest.UserRest
import ru.nudlik.sdrinksclient.ui.auth.AuthViewModel
import ru.nudlik.sdrinksclient.ui.recipes.RecipesViewModel
import javax.inject.Singleton

@Singleton
@Component(dependencies = [], modules = [
    AppModule::class,
    AppDatabaseModule::class,
    RestUtilModule::class])
interface AppComponent {
    fun inject(mainViewModel: MainViewModel)
    fun inject(authViewModel: AuthViewModel)
    fun inject(recipeViewModel: RecipesViewModel)
    fun userEntityDao(): UserEntityDao
    fun userEntityRepository(): UserEntityRepository
    fun likedRecipesDao(): LikedRecipesDao
    fun likedRecipesRepository(): LikedRecipesRepository
    fun application(): Application
    fun getOkHttpClient(): OkHttpClient
    fun getBasicAuthInterceptor(): BasicAuthInterceptor
    fun getRecipesRest(): RecipesRest
    fun getUserRest(): UserRest
    fun getCommentRest(): CommentRest
    fun getResponseHandler(): ResponseHandler
    fun getPicasso(): Picasso
}