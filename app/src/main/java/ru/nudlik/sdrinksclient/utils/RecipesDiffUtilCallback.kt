package ru.nudlik.sdrinksclient.utils

import androidx.recyclerview.widget.DiffUtil
import ru.nudlik.sdrinksclient.model.recipeitem.RecipeItem

class RecipesDiffUtilCallback() : DiffUtil.ItemCallback<RecipeItem>() {
    override fun areItemsTheSame(oldItem: RecipeItem, newItem: RecipeItem): Boolean {
        var bb = oldItem.items.size == newItem.items.size
        bb = bb && oldItem.typeRow == newItem.typeRow
        if (oldItem.items.size > 0 && bb) {
            bb = bb && oldItem.items[0]?.recipeId == newItem.items[0]?.recipeId
        }
        if (oldItem.items.size > 1 && bb) {
            bb = bb && oldItem.items[1]?.recipeId == newItem.items[1]?.recipeId
        }
        if (oldItem.items.size > 2 && bb) {
            bb = bb && oldItem.items[2]?.recipeId == newItem.items[2]?.recipeId
        }
        return bb
    }

    override fun areContentsTheSame(oldItem: RecipeItem, newItem: RecipeItem): Boolean {
        return oldItem == newItem
    }
}