package ru.nudlik.sdrinksclient

import android.app.SearchManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import ru.nudlik.fragment.bottomsheet.CustomizeSearchBSDialog
import ru.nudlik.sdrinksclient.databinding.MainActivityBinding
import ru.nudlik.sdrinksclient.extentions.findFragmentByClass
import ru.nudlik.sdrinksclient.extentions.gone
import ru.nudlik.sdrinksclient.extentions.visible
import ru.nudlik.sdrinksclient.ui.auth.AuthFragment
import ru.nudlik.sdrinksclient.ui.widget.CustomSearchView

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    private lateinit var customizeSearchItem: MenuItem
    private lateinit var viewModel: MainViewModel
    private lateinit var dataBinding: MainActivityBinding

    private val showHideSearchListener = object : CustomSearchView.OnCollapsedExpandedListener {
        override fun onCollapsed() {
            customizeSearchItem.isVisible = false
            dataBinding.appBarMain.createNewRecipeFab.setImageDrawable(getDrawable(R.drawable.ic_add_white_24dp))
            dataBinding.appBarMain.createNewRecipeFab.setOnClickListener(viewModel::onCreateNewRecipe)
        }

        override fun onExpanded() {
            customizeSearchItem.isVisible = true
            dataBinding.appBarMain.createNewRecipeFab.setImageDrawable(getDrawable(R.drawable.ic_search_white_24dp))
            dataBinding.appBarMain.createNewRecipeFab.setOnClickListener(viewModel::onSearch)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this@MainActivity).get(MainViewModel::class.java)
        SDrinksApplication.getInstance().getAppComponent().inject(viewModel)
        dataBinding = DataBindingUtil.setContentView(this@MainActivity, R.layout.main_activity)
        dataBinding.mainViewModel = viewModel
        viewModel.dataBinding = dataBinding
        setSupportActionBar(dataBinding.appBarMain.toolbarMain)
        dataBinding.appBarMain.collapsingToolbarMain.isTitleEnabled = false
        val toggle = ActionBarDrawerToggle(
            this, dataBinding.drawerLayout, dataBinding.appBarMain.toolbarMain, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        dataBinding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navController = Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment)
        viewModel.navController = navController
        NavigationUI.setupWithNavController(dataBinding.navView, navController)
        NavigationUI.setupActionBarWithNavController(this, navController, dataBinding.drawerLayout)
        dataBinding.appBarMain.swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
            android.R.color.holo_green_light, android.R.color.holo_orange_light,
            android.R.color.holo_red_light)
        dataBinding.appBarMain.swipeRefresh.setProgressViewOffset(false, 0, 100)

        navController.addOnDestinationChangedListener(viewModel::onDestinationChange)

        viewModel.initialize(this@MainActivity)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_toolbar, menu)
        viewModel.searchView =  menu?.findItem(R.id.action_search)?.actionView as CustomSearchView
        customizeSearchItem = menu.findItem(R.id.action_customize_search)
        viewModel.searchView.setOnCollapsedExpandedListener(showHideSearchListener)
        initSearchableInfo()
        return true
    }

    private fun initSearchableInfo() {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(
//            ComponentName(this@MainActivity,
//                SearchResultsActivity::class.java)
//        ))
        viewModel.searchView.setOnQueryTextListener(null)
        customizeSearchItem.setOnMenuItemClickListener {
            val cbs = CustomizeSearchBSDialog.createInstance()
            cbs.show(supportFragmentManager, CustomizeSearchBSDialog.TAG)
            true
        }
    }

    override fun onBackPressed() {
        when (navController.currentDestination?.id) {
            R.id.authFragment -> {
                val authFrag = supportFragmentManager.findFragmentByClass<AuthFragment>()
                authFrag?.apply {
                    if (!(this.onBackPressed())) {
                        super.onBackPressed()
                    }
//                    if (!(this is AuthFragment)this.onBackPressed()) {
//
//                    }
                }
            }
            else -> {
                super.onBackPressed()
            }
        }
    }
}
//fun FragmentManager.findCurrentNavigationFragment(): Fragment? = primaryNavigationFragment?.childFragmentManager?.fragments?.firstOrNull()