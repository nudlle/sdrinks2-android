package ru.nudlik.sdrinksclient

import android.app.Application
import android.content.res.ColorStateList
import ru.nudlik.sdrinksclient.model.database.AppDatabaseModule
import ru.nudlik.sdrinksclient.model.http.module.RestUtilModule

class SDrinksApplication : Application() {
//    private lateinit var instance: ru.nudlik.sdrinksclient.SDrinksApplication
    private lateinit var dagger: AppComponent

    override fun onCreate() {
        super.onCreate()
        instance = this
        dagger = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .appDatabaseModule(AppDatabaseModule(this))
            .restUtilModule(RestUtilModule(this))
            .build()
    }

    fun getAppComponent(): AppComponent {
        return dagger
    }

    companion object {
        private lateinit var instance: SDrinksApplication
        fun getInstance(): SDrinksApplication {
            return instance
        }
    }
}