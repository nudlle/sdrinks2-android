package ru.nudlik.fragment.bottomsheet

import android.content.Context
import com.google.android.material.bottomsheet.BottomSheetDialog

class BottomSheetDialogCustom(context: Context, theme: Int) : BottomSheetDialog(context, theme) {
    override fun onBackPressed() {
        dismiss()
    }
}