package ru.nudlik.sdrinksclient.ui.auth

import android.util.Patterns
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import ru.nudlik.fragment.snackbar.SnackbarMaker
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.SDrinksApplication
import ru.nudlik.sdrinksclient.databinding.AuthFragmentBinding
import ru.nudlik.sdrinksclient.extentions.gone
import ru.nudlik.sdrinksclient.extentions.invisible
import ru.nudlik.sdrinksclient.extentions.visible
import ru.nudlik.sdrinksclient.listener.EditTextErrorCleaner
import ru.nudlik.sdrinksclient.model.database.repository.UserEntityRepository
import ru.nudlik.sdrinksclient.model.domain.User
import ru.nudlik.sdrinksclient.model.http.response.ServerResponseCode
import ru.nudlik.sdrinksclient.model.http.rest.ResponseCallback
import ru.nudlik.sdrinksclient.model.http.rest.ResponseHandler
import ru.nudlik.sdrinksclient.model.http.rest.UserRest
import ru.nudlik.sdrinksclient.model.inner.domain.UserEntity
import ru.nudlik.sdrinksclient.security.CryptoUtil
import ru.nudlik.sdrinksclient.security.CryptoUtilImpl
import ru.nudlik.sdrinksclient.security.UserRequest
import java.net.ConnectException
import java.net.SocketTimeoutException
import javax.inject.Inject

class AuthViewModel : ViewModel() {
    var email: String = ""
    var pass: String = ""
    var emailCheck: String = ""
    var confirmPass: String = ""
    lateinit var authDataFragmentBinding: AuthFragmentBinding
    @Inject
    lateinit var userRepository: UserEntityRepository
    @Inject
    lateinit var userRest: UserRest
    @Inject
    lateinit var responseHandler: ResponseHandler
    lateinit var navController: NavController
    lateinit var user: User
    private var state: State = State.LOGIN
    private val cryptoUtil: CryptoUtil = CryptoUtilImpl()

    private fun showLoading(show: Boolean) {
        if (show) {
            authDataFragmentBinding.edittextHolder.visibility = View.GONE
            authDataFragmentBinding.loginProgressBar.visibility = View.VISIBLE
            authDataFragmentBinding.loginProgressBar.show()
        } else {
            authDataFragmentBinding.loginProgressBar.hide()
            authDataFragmentBinding.loginProgressBar.visibility = View.GONE
            authDataFragmentBinding.edittextHolder.visibility = View.VISIBLE
        }
    }
    private fun actionState(newState: State) {
        when (state) {
            State.LOGIN -> {
                when (newState) {
                    State.RECOVER -> {
                        state = newState
                        authDataFragmentBinding.recoverButton.backgroundTintList =
                            SDrinksApplication.getInstance().getColorStateList(R.color.colorAccent)
                        authDataFragmentBinding.loginButton.backgroundTintList =
                            SDrinksApplication.getInstance().getColorStateList(android.R.color.transparent)
                        authDataFragmentBinding.recoverTitle.visible(200)
                        authDataFragmentBinding.registerButton.gone(200)
                        authDataFragmentBinding.loginTitle.gone(200)
                        authDataFragmentBinding.passwordInputLayout.invisible(200)
                        authDataFragmentBinding.passwordInputLayout.error = null
                        pass = ""
                        authDataFragmentBinding.confirmPassword.error = null
                        confirmPass = ""
                    }
                    State.REGISTER -> {
                        state = newState
                        authDataFragmentBinding.registerButton.backgroundTintList =
                            SDrinksApplication.getInstance().getColorStateList(R.color.colorAccent)
                        authDataFragmentBinding.registerTitle.visible(200)
                        authDataFragmentBinding.confirmPasswordInputLayout.visible(200)
                        authDataFragmentBinding.loginTitle.gone(200)
                        authDataFragmentBinding.loginButtonsHolder.gone(200)
                        authDataFragmentBinding.passwordInputLayout.error = null
                        pass = ""
                    }
                    else -> {
                        makeLoginAction()
                    }
                }
            }
            State.RECOVER -> {
                when (newState) {
                    State.RECOVER -> {
                        makeRecoverAction()
                    }
                    else -> {
                        state = newState
                        authDataFragmentBinding.loginButton.backgroundTintList =
                            SDrinksApplication.getInstance().getColorStateList(R.color.colorAccent)
                        authDataFragmentBinding.recoverButton.backgroundTintList =
                            SDrinksApplication.getInstance().getColorStateList(android.R.color.transparent)
                        authDataFragmentBinding.recoverTitle.gone(200)
                        authDataFragmentBinding.loginTitle.visible(200)
                        authDataFragmentBinding.passwordInputLayout.visible(200)
                        authDataFragmentBinding.registerButton.visible(200)
                    }
                }
            }
            State.REGISTER -> {
                makeRegisterAction()
            }
            State.REGISTER_CHECK_EMAIL -> {
                if (valid()) {
                    val user = User()
                    user.email = email
                    user.password = pass
                    val userRequest = UserRequest()
                    userRequest.user = user
                    userRequest.timestamp = System.currentTimeMillis()
                    showLoading(true)
                    authDataFragmentBinding.confirmRecoverButton.gone()
                    userRest.registerUser(email, emailCheck, userRequest, registerUserCallback)
                }
            }
            State.RECOVER_CHECK_EMAIL -> {
                confirmRecoverAction()
            }
        }
    }

    private val registerUserCallback = object : ResponseCallback {
        override fun <T> success(data: T?) {
            if (data is User) {
                saveUser(data as User)
            }
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            authDataFragmentBinding.emailCheckInputLayout.error = SDrinksApplication.getInstance().getString(responseHandler.getStringResponseRes(responseCode))
            showLoading(false)
            authDataFragmentBinding.confirmRecoverButton.visible()
        }

        override fun exception(e: Throwable?) {
            super.exception(e)
            showLoading(false)
            authDataFragmentBinding.confirmRecoverButton.visible()
        }
    }

    private fun validateEmail(): Boolean {
        return when {
            email.isEmpty() -> {
                authDataFragmentBinding.emailInputLayout.error = SDrinksApplication.getInstance().getString(R.string.not_be_empty)
                false
            }
            email.length > 30 -> {
                authDataFragmentBinding.emailInputLayout.error = SDrinksApplication.getInstance().getString(R.string.max_length_error)
                false
            }
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                authDataFragmentBinding.emailInputLayout.error = SDrinksApplication.getInstance().getString(R.string.invalid_email_error)
                false
            }
            else -> {
                true
            }
        }
    }
    private fun validatePwd(): Boolean {
        return when {
            pass.isEmpty() -> {
                authDataFragmentBinding.passwordInputLayout.error = SDrinksApplication.getInstance().getString(R.string.not_be_empty)
                false
            }
            pass.length > 20 -> {
                authDataFragmentBinding.passwordInputLayout.error = SDrinksApplication.getInstance().getString(R.string.max_length_error)
                false
            }
            else -> {
                true
            }
        }
    }
    private fun validateConfirmPwd(): Boolean {
        return when {
            confirmPass.isEmpty() -> {
                authDataFragmentBinding.confirmPasswordInputLayout.error = SDrinksApplication.getInstance().getString(R.string.not_be_empty)
                false
            }
            confirmPass.length > 20 -> {
                authDataFragmentBinding.confirmPasswordInputLayout.error = SDrinksApplication.getInstance().getString(R.string.max_length_error)
                false
            }
            confirmPass != pass -> {
                authDataFragmentBinding.confirmPasswordInputLayout.error = SDrinksApplication.getInstance().getString(R.string.password_not_match_error)
                false
            }
            else -> {
                true
            }
        }
    }
    private fun validateCheckEmailHash(): Boolean {
        return when {
            emailCheck.isEmpty() -> {
                authDataFragmentBinding.emailCheckInputLayout.error = SDrinksApplication.getInstance().getString(R.string.not_be_empty)
                false
            }
            emailCheck.length > 40 -> {
                authDataFragmentBinding.emailCheckInputLayout.error = SDrinksApplication.getInstance().getString(R.string.max_length_error)
                false
            }
            else -> {
                true
            }
        }
    }
    private fun valid(): Boolean {
        return when (state) {
            State.LOGIN -> {
                validateEmail() && validatePwd()
            }
            State.RECOVER -> {
                validateEmail()
            }
            State.RECOVER_CHECK_EMAIL -> {
                validateCheckEmailHash() && validatePwd() && validateConfirmPwd()
            }
            State.REGISTER -> {
                validateEmail() && validatePwd() && validateConfirmPwd()
            }
            State.REGISTER_CHECK_EMAIL -> {
                validateCheckEmailHash()
            }
        }
    }
    fun handleBackPressed(): Boolean {
        when (state) {
            State.REGISTER -> {
                state = State.LOGIN
                authDataFragmentBinding.registerButton.backgroundTintList = SDrinksApplication.getInstance().getColorStateList(android.R.color.transparent)
                authDataFragmentBinding.registerTitle.gone(200)
                authDataFragmentBinding.confirmPasswordInputLayout.gone(200)
                authDataFragmentBinding.loginTitle.visible(200)
                authDataFragmentBinding.loginButtonsHolder.visible(200)
                authDataFragmentBinding.passwordInputLayout.error = null
                authDataFragmentBinding.passwordInputLayout.editText?.setText("")
                pass = ""
                authDataFragmentBinding.confirmPasswordInputLayout.error = null
                authDataFragmentBinding.confirmPasswordInputLayout.editText?.setText("")
                confirmPass = ""
            }
            State.RECOVER -> {
                state = State.LOGIN
                authDataFragmentBinding.recoverButton.backgroundTintList = SDrinksApplication.getInstance().getColorStateList(android.R.color.transparent)
                authDataFragmentBinding.loginButton.backgroundTintList = SDrinksApplication.getInstance().getColorStateList(R.color.colorAccent)
                authDataFragmentBinding.recoverTitle.gone(200)
                authDataFragmentBinding.loginTitle.visible(200)
                authDataFragmentBinding.passwordInputLayout.visible(200)
                authDataFragmentBinding.registerButton.visible(200)
            }
            State.REGISTER_CHECK_EMAIL -> {
                state = State.REGISTER
                emailCheck = ""
                authDataFragmentBinding.emailCheckInputLayout.editText?.setText("")
                authDataFragmentBinding.emailCheckInputLayout.error = null
                authDataFragmentBinding.emailCheckInputLayout.gone(200)
                authDataFragmentBinding.confirmRecoverButton.gone(200)
                authDataFragmentBinding.emailInputLayout.visible(200)
                authDataFragmentBinding.passwordInputLayout.visible(200)
                authDataFragmentBinding.confirmPasswordInputLayout.visible(200)
                authDataFragmentBinding.registerButton.visible(200)
            }
            State.RECOVER_CHECK_EMAIL -> {
                state = State.RECOVER
                authDataFragmentBinding.emailInputLayout.visible(200)
                authDataFragmentBinding.loginButtonsHolder.visible(200)
                emailCheck = ""
                authDataFragmentBinding.emailCheckInputLayout.editText?.setText("")
                authDataFragmentBinding.emailCheckInputLayout.error = null
                authDataFragmentBinding.emailCheckInputLayout.gone()
                pass = ""
                authDataFragmentBinding.passwordInputLayout.editText?.setText("")
                authDataFragmentBinding.passwordInputLayout.error = null
                authDataFragmentBinding.passwordInputLayout.invisible()
                confirmPass = ""
                authDataFragmentBinding.confirmPasswordInputLayout.editText?.setText("")
                authDataFragmentBinding.confirmPasswordInputLayout.error = null
                authDataFragmentBinding.confirmPasswordInputLayout.gone()
                authDataFragmentBinding.confirmRecoverButton.gone()
            }
            else -> {
                return false
            }
        }
        return true
    }

    private fun saveUser(user: User) {
        user.password = cryptoUtil.encode(CryptoUtil.PIN_ALIAS, pass)
        val userEntity = UserEntity()
        userEntity.user = user
        Thread {
            userRepository.putUserEntity(userEntity)
        }.start()
        navController.popBackStack()
    }

    private val loginWithPassword = object : ResponseCallback {
        override fun <T> success(data: T?) {
            if (data is User) {
                saveUser(data as User)
            }
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            showLoading(false)
            authDataFragmentBinding.loginButtonsHolder.visible()
            authDataFragmentBinding.registerButton.visible()
            val text = SDrinksApplication.getInstance().getString(responseHandler.getStringResponseRes(responseCode))
            if (responseCode == ServerResponseCode.LOGIN_FAILED) {
                authDataFragmentBinding.passwordInputLayout.error = text
            } else {
                authDataFragmentBinding.emailInputLayout.error = text
            }
        }

        override fun exception(e: Throwable?) {
            super.exception(e)
            showLoading(false)
            authDataFragmentBinding.loginButtonsHolder.visible()
            authDataFragmentBinding.registerButton.visible()
            handleError(e)
        }
    }

    private fun handleError(e: Throwable?) {
        when (e) {
            is SocketTimeoutException ->
                SnackbarMaker.showLongSnackbar(authDataFragmentBinding.authParentView, R.string.timeout_error_title)
            is ConnectException -> {
                SnackbarMaker.showLongSnackbar(authDataFragmentBinding.authParentView, R.string.connect_error_title)
            }
            else -> {
                SnackbarMaker.showLongSnackbar(authDataFragmentBinding.authParentView, R.string.unknown_exception_title)
            }
        }
    }

    private fun makeLoginAction() {
        if (valid()) {
            authDataFragmentBinding.loginButtonsHolder.gone()
            authDataFragmentBinding.registerButton.gone()
            showLoading(true)
            val user = User()
            user.email = email
            user.password = pass
            val userRequest = UserRequest()
            userRequest.user = user
            userRequest.timestamp = System.currentTimeMillis()
            userRest.loginWithPassword(userRequest, loginWithPassword)
        }
    }

    private val recoverCallback = object : ResponseCallback {
        override fun <T> success(data: T?) {
            state = State.RECOVER_CHECK_EMAIL
            showLoading(false)
            authDataFragmentBinding.emailCheckInputLayout.visible(200)
            authDataFragmentBinding.passwordInputLayout.visible(200)
            authDataFragmentBinding.confirmPasswordInputLayout.visible(200)
            authDataFragmentBinding.confirmRecoverButton.visible(200)
            authDataFragmentBinding.emailInputLayout.gone()
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            showLoading(false)
            authDataFragmentBinding.loginButtonsHolder.visible()
            authDataFragmentBinding.emailInputLayout.error =
                SDrinksApplication.getInstance().getString(responseHandler.getStringResponseRes(responseCode))
        }

        override fun exception(e: Throwable?) {
            super.exception(e)
            showLoading(false)
            authDataFragmentBinding.loginButtonsHolder.visible()
            handleError(e)
        }
    }

    private fun makeRecoverAction() {
        if (valid()) {
            authDataFragmentBinding.loginButtonsHolder.gone()
            showLoading(true)
            userRest.checkEmailOnRecoverRequest(email, recoverCallback)
        }
    }

    private val registerCallback = object : ResponseCallback {
        override fun <T> success(data: T?) {
            showLoading(false)
            state = State.REGISTER_CHECK_EMAIL
            authDataFragmentBinding.confirmRecoverButton.visible(200)
            authDataFragmentBinding.emailCheckInputLayout.visible(200)
            authDataFragmentBinding.emailInputLayout.gone()
            authDataFragmentBinding.passwordInputLayout.gone()
            authDataFragmentBinding.confirmPasswordInputLayout.gone()
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            showLoading(false)
            authDataFragmentBinding.registerButton.visible(200)
            authDataFragmentBinding.emailInputLayout.error =
                SDrinksApplication.getInstance().getString(responseHandler.getStringResponseRes(responseCode))
            state = State.REGISTER
        }

        override fun exception(e: Throwable?) {
            super.exception(e)
            showLoading(false)
            authDataFragmentBinding.registerButton.visible(200)
            state = State.REGISTER
            handleError(e)
        }
    }

    private fun makeRegisterAction() {
        if (valid()) {
            showLoading(true)
            authDataFragmentBinding.registerButton.gone(200)
            userRest.checkEmailOnRegisterRequest(email, registerCallback)
        }
    }

    private val confirmRecoverCallback = object : ResponseCallback {
        override fun <T> success(data: T?) {
            saveUser(data as User)
        }

        override fun errorCode(responseCode: ServerResponseCode?) {
            showLoading(false)
            authDataFragmentBinding.confirmRecoverButton.visible(200)
            authDataFragmentBinding.emailCheckInputLayout.visible(200)
            authDataFragmentBinding.passwordInputLayout.visible(200)
            authDataFragmentBinding.confirmPasswordInputLayout.visible(200)
            authDataFragmentBinding.emailCheckInputLayout.error =
                SDrinksApplication.getInstance().getString(responseHandler.getStringResponseRes(responseCode))
        }

        override fun exception(e: Throwable?) {
            super.exception(e)
            showLoading(false)
            authDataFragmentBinding.confirmRecoverButton.visible(200)
            authDataFragmentBinding.emailCheckInputLayout.visible(200)
            authDataFragmentBinding.passwordInputLayout.visible(200)
            authDataFragmentBinding.confirmPasswordInputLayout.visible(200)
            handleError(e)
        }
    }

    private fun confirmRecoverAction() {
        if (valid()) {
            showLoading(true)
            authDataFragmentBinding.confirmRecoverButton.gone()
            val user = User()
            user.email = email
            user.password = confirmPass
            val userRequest = UserRequest()
            userRequest.user = user
            userRequest.timestamp = System.currentTimeMillis()
            userRest.recoverWithEmail(emailCheck, userRequest, confirmRecoverCallback)
        }
    }

    fun login(view: View?) {
        actionState(State.LOGIN)
    }
    fun register(view: View?) {
        actionState(State.REGISTER)
    }
    fun recover(view: View?) {
        actionState(State.RECOVER)
    }
    fun confirmRecover(view: View?) {
        actionState(state)
    }

    fun initialize() {
        authDataFragmentBinding.emailInputLayout.editText?.
            addTextChangedListener(EditTextErrorCleaner(authDataFragmentBinding.emailInputLayout))
        authDataFragmentBinding.passwordInputLayout.editText?.
            addTextChangedListener(EditTextErrorCleaner(authDataFragmentBinding.passwordInputLayout))
        authDataFragmentBinding.confirmPasswordInputLayout.editText?.
            addTextChangedListener(EditTextErrorCleaner(authDataFragmentBinding.confirmPasswordInputLayout))
        authDataFragmentBinding.emailCheckInputLayout.editText?.
            addTextChangedListener(EditTextErrorCleaner(authDataFragmentBinding.emailCheckInputLayout))
        authDataFragmentBinding.loginButton.backgroundTintList =
            SDrinksApplication.getInstance().getColorStateList(R.color.colorAccent)
    }

    enum class State {
        LOGIN, RECOVER, RECOVER_CHECK_EMAIL, REGISTER, REGISTER_CHECK_EMAIL
    }

    override fun onCleared() {
        userRest.destroy()
    }
}