package ru.nudlik.sdrinksclient.ui.recipes

import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import ru.nudlik.sdrinksclient.databinding.RecipesFragmentBinding
import ru.nudlik.sdrinksclient.model.http.rest.RecipesRest
import ru.nudlik.sdrinksclient.model.recipeitem.RecipeItem
import ru.nudlik.sdrinksclient.ui.recipes.adapter.RecipeItemsPosDataSource
import ru.nudlik.sdrinksclient.ui.recipes.adapter.RecipesPagedListAdapter
import ru.nudlik.sdrinksclient.utils.MainThreadExecutor
import ru.nudlik.sdrinksclient.utils.RecipesDiffUtilCallback
import java.util.concurrent.Executors
import javax.inject.Inject

class RecipesViewModel : ViewModel() {
    lateinit var pageList: PagedList<RecipeItem>
    lateinit var dataSource: RecipeItemsPosDataSource
    lateinit var dataBinding: RecipesFragmentBinding
    lateinit var adapter: RecipesPagedListAdapter
    private val recipeDiffUtil = RecipesDiffUtilCallback()

    @Inject
    lateinit var recipesRest: RecipesRest

    fun initialize() {
        dataSource = RecipeItemsPosDataSource(recipesRest)
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(10)
            .setPageSize(5)
            .build()

        pageList = PagedList.Builder(dataSource, config)
            .setFetchExecutor(Executors.newSingleThreadExecutor())
            .setNotifyExecutor(MainThreadExecutor())
            .build()

        adapter = RecipesPagedListAdapter(recipeDiffUtil)
        adapter.submitList(pageList)
        dataBinding.recipeRecyclerView.adapter = adapter
    }

    override fun onCleared() {
        recipesRest.destroy()
    }
}
