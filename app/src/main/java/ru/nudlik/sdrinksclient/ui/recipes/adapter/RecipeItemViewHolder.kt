package ru.nudlik.sdrinksclient.ui.recipes.adapter

import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.databinding.RvItemRecipeContentBinding
import ru.nudlik.sdrinksclient.extentions.gone
import ru.nudlik.sdrinksclient.extentions.loadUri
import ru.nudlik.sdrinksclient.extentions.visible
import ru.nudlik.sdrinksclient.model.http.RestClient
import ru.nudlik.sdrinksclient.model.recipeitem.RecipeItem

class RecipeItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//    private val simpleItemContainer: LinearLayout = view.findViewById(R.id.recipe_item_simple_row)
//    private val simpleFirstImageHolder: CardView = view.findViewById(R.id.simple_first_item_holder)
//    private val simpleFirstImage: ImageView = view.findViewById(R.id.simple_item_first)
//    private val simpleSecondImageHolder: CardView = view.findViewById(R.id.simple_second_item_holder)
//    private val simpleSecondImage: ImageView = view.findViewById(R.id.simple_item_second)
//    private val simpleThirdImageHolder: CardView = view.findViewById(R.id.simple_third_item_holder)
//    private val simpleThirdImage: ImageView = view.findViewById(R.id.simple_item_third)
//
//    private val bigLeftItemContainer: LinearLayout = view.findViewById(R.id.recipe_item_big_left_row)
//    private val bigLeftFirstImageHolder: CardView = view.findViewById(R.id.big_left_first_item_holder)
//    private val bigLeftFirstImage: ImageView = view.findViewById(R.id.big_left_item_first)
//    private val bigLeftSecondImageHolder: CardView = view.findViewById(R.id.big_left_second_item_holder)
//    private val bigLeftSecondImage: ImageView = view.findViewById(R.id.big_left_item_second)
//    private val bigLeftThirdImageHolder: CardView = view.findViewById(R.id.big_left_third_item_holder)
//    private val bigLeftThirdImage: ImageView = view.findViewById(R.id.big_left_item_third)
//
//    private val bigRightItemContainer: LinearLayout = view.findViewById(R.id.recipe_item_big_right_row)
//    private val bigRightFirstImageHolder: CardView = view.findViewById(R.id.big_right_first_item_holder)
//    private val bigRightFirstImage: ImageView = view.findViewById(R.id.big_right_item_first)
//    private val bigRightSecondImageHolder: CardView = view.findViewById(R.id.big_right_second_item_holder)
//    private val bigRightSecondImage: ImageView = view.findViewById(R.id.big_right_item_second)
//    private val bigRightThirdImageHolder: CardView = view.findViewById(R.id.big_right_third_item_holder)
//    private val bigRightThirdImage: ImageView = view.findViewById(R.id.big_right_item_third)

//    private var itemClickListener: ItemClickListener? = null
    val recipeItemDataBinding: RvItemRecipeContentBinding = DataBindingUtil.bind(view)!!

//    fun addListener(l: ItemClickListener?) {
//        this.itemClickListener = l
//    }


    fun replaceMargins() {
        val dimsInDp = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            136f, itemView.resources.displayMetrics)
        val params = LinearLayout.LayoutParams(dimsInDp.toInt(), dimsInDp.toInt())
        val margin = itemView.resources.getDimension(R.dimen.recipe_item_main_view_margin).toInt()
        params.topMargin = margin
        params.bottomMargin = margin
        recipeItemDataBinding.simpleFirstItemHolder.layoutParams = params
        recipeItemDataBinding.simpleSecondItemHolder.layoutParams = params
        recipeItemDataBinding.simpleThirdItemHolder.layoutParams = params
    }

    fun bind(recipeItem: RecipeItem?) {
        when (recipeItem!!.typeRow) {
            RecipeItem.SIMPLE_ROW -> {
                recipeItemDataBinding.recipeItemSimpleRow.visible()
                recipeItemDataBinding.recipeItemBigLeftRow.gone()
                recipeItemDataBinding.recipeItemBigRightRow.gone()
//                simpleItemContainer.visible()
//                bigLeftItemContainer.gone()
//                bigRightItemContainer.gone()
//
//                when (recipeItem.items.size) {
//                    1 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.small,
//                            simpleFirstImage, simpleFirstImageHolder, 0)
//                    }
//                    2 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.small,
//                            simpleFirstImage, simpleFirstImageHolder, 0)
//                        loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.small,
//                            simpleSecondImage, simpleSecondImageHolder, 1)
//                    }
//                    3 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.small,
//                            simpleFirstImage, simpleFirstImageHolder, 0)
//                        loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.small,
//                            simpleSecondImage, simpleSecondImageHolder, 1)
//                        loadImages(recipeItem.items[2], recipeItem.items[2]?.imageUrls?.small,
//                            simpleThirdImage, simpleThirdImageHolder, 2)
//                    }
//                }
//
            }
            RecipeItem.LEFT_ROW -> {
                recipeItemDataBinding.recipeItemSimpleRow.gone()
                recipeItemDataBinding.recipeItemBigLeftRow.visible()
                recipeItemDataBinding.recipeItemBigRightRow.gone()
//                simpleItemContainer.gone()
//                bigLeftItemContainer.visible()
//                bigRightItemContainer.gone()
//
//                when (recipeItem.items.size) {
//                    1 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
//                            bigLeftFirstImage, bigLeftFirstImageHolder, 0)
//                    }
//                    2 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
//                            bigLeftFirstImage, bigLeftFirstImageHolder, 0)
//                        loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.small,
//                            bigLeftSecondImage, bigLeftSecondImageHolder, 1)
//                    }
//                    3 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.medium,
//                            bigLeftFirstImage, bigLeftFirstImageHolder, 0)
//                        loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.small,
//                            bigLeftSecondImage, bigLeftSecondImageHolder, 1)
//                        loadImages(recipeItem.items[2], recipeItem.items[2]?.imageUrls?.small,
//                            bigLeftThirdImage, bigLeftThirdImageHolder, 2)
//                    }
//                }
//
            }
            RecipeItem.RIGHT_ROW -> {
                recipeItemDataBinding.recipeItemSimpleRow.gone()
                recipeItemDataBinding.recipeItemBigLeftRow.gone()
                recipeItemDataBinding.recipeItemBigRightRow.visible()
//                simpleItemContainer.gone()
//                bigLeftItemContainer.gone()
//                bigRightItemContainer.visible()
//
//                when (recipeItem.items.size) {
//                    1 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.small,
//                            bigRightFirstImage, bigRightFirstImageHolder, 0)
//                    }
//                    2 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.small,
//                            bigRightFirstImage, bigRightFirstImageHolder, 0)
//                        loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.small,
//                            bigRightSecondImage, bigRightSecondImageHolder, 1)
//                    }
//                    3 -> {
//                        loadImages(recipeItem.items[0], recipeItem.items[0]?.imageUrls?.small,
//                            bigRightFirstImage, bigRightFirstImageHolder, 0)
//                        loadImages(recipeItem.items[1], recipeItem.items[1]?.imageUrls?.small,
//                            bigRightSecondImage, bigRightSecondImageHolder, 1)
//                        loadImages(recipeItem.items[2], recipeItem.items[2]?.imageUrls?.medium,
//                            bigRightThirdImage, bigRightThirdImageHolder, 2)
//                    }
                }
            }
//        }
//        if (adapterPosition == 0) {
//            val dimsInDp = TypedValue.applyDimension(
//                TypedValue.COMPLEX_UNIT_DIP,
//                136f, itemView.resources.displayMetrics)
//            val params = LinearLayout.LayoutParams(dimsInDp.toInt(), dimsInDp.toInt())
//            val margin = itemView.resources.getDimension(R.dimen.recipe_item_main_view_margin).toInt()
//            params.topMargin = margin
//            params.bottomMargin = margin
//            recipeItemDataBinding.simpleFirstItemHolder.layoutParams = params
//            recipeItemDataBinding.simpleSecondItemHolder.layoutParams = params
//            recipeItemDataBinding.simpleThirdItemHolder.layoutParams = params
//            simpleFirstImageHolder.layoutParams = params
//            simpleSecondImageHolder.layoutParams = params
//            simpleThirdImageHolder.layoutParams = params
//        }
    }


//    private fun loadImages(recipe: Recipe?,
//                           url: String?,
//                           imageView: ImageView,
//                           cardView: CardView,
//                           correction: Int) {
//        if (recipe != null) {
//            imageView.loadUri("${RestClient.URL}/resources/recipes/${recipe.recipeId}/$url")
//            cardView.setOnClickListener {
//                itemClickListener?.onItemClick(it, adapterPosition, correction)
//            }
//        } else {
//            cardView.invisible()
//        }
//    }

    companion object {
        @BindingAdapter("app:loadUrlId", "app:loadUrl")
        @JvmStatic
        fun loadImage(imageView: ImageView, id: String?, url: String?) {
            if (id != null && url != null) {
                imageView.loadUri("${RestClient.URL}/resources/recipes/${id}/${url}")
            }
        }
    }
}