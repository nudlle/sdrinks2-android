package ru.nudlik.sdrinksclient.ui.create

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ru.nudlik.sdrinksclient.R

class CreateRecipeFragment : Fragment() {
    companion object {
        fun newInstance() = CreateRecipeFragment()
    }

    private lateinit var viewModel: CreateRecipeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.liked_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CreateRecipeViewModel::class.java)
        // TODO: Use the ViewModel
    }
}