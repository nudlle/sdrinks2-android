package ru.nudlik.sdrinksclient.ui.recipes.adapter

import androidx.paging.PositionalDataSource
import ru.nudlik.sdrinksclient.model.domain.Recipe
import ru.nudlik.sdrinksclient.model.http.response.ServerResponseCode
import ru.nudlik.sdrinksclient.model.http.rest.RecipesRest
import ru.nudlik.sdrinksclient.model.http.rest.ResponseCallback
import ru.nudlik.sdrinksclient.model.recipeitem.RecipeItem
import ru.nudlik.sdrinksclient.utils.RecipeUtil

class RecipeItemsPosDataSource(private val recipesRest: RecipesRest) : PositionalDataSource<RecipeItem>() {
    val recipeItems = ArrayList<RecipeItem>()

    private fun calcPageNum(loadSize: Int): Int {
        if (recipeItems.size < loadSize) return 0
        return (recipeItems.size / loadSize) - 1
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<RecipeItem>) {
        if (recipeItems.size == 0) return
        val pageNum = calcPageNum(params.loadSize)
        recipesRest.getRecipes(pageNum, params.loadSize * 3, object : ResponseCallback {
            override fun <T> success(data: T?) {
                if (data is List<*>) {
                    val list = data.filterIsInstance<Recipe>()
                    if (list.isNotEmpty()) {
                        val converted = when {
                            recipeItems.size >= 2 -> RecipeUtil.convert(list, recipeItems[recipeItems.size - 1], recipeItems[recipeItems.size - 2])
                            recipeItems.size >= 1 -> RecipeUtil.convert(list, recipeItems[recipeItems.size - 1], null)
                            else -> RecipeUtil.convert(list, null, null)
                        }
                        converted.forEach { recipeItems.add(it) }
                        callback.onResult(converted)
                    } else {
                        callback.onResult(emptyList())
                    }
                }
            }

            override fun errorCode(responseCode: ServerResponseCode?) {
                callback.onResult(emptyList())
            }

            override fun exception(e: Throwable?) {
                callback.onResult(emptyList())
            }
        })
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<RecipeItem>) {
        recipesRest.getRecipes(params.requestedStartPosition, params.requestedLoadSize * 3, object : ResponseCallback {
            override fun <T> success(data: T?) {
                if (data is List<*>) {
                    val ll = RecipeUtil.convert(data.filterIsInstance<Recipe>(), null, null)
                    recipeItems.addAll(ll)
                    callback.onResult(ll, 0)
                }
            }

            override fun errorCode(responseCode: ServerResponseCode?) {
                callback.onResult(emptyList(), 0)
            }

            override fun exception(e: Throwable?) {
                callback.onResult(emptyList(), 0)
            }
        })
    }
}