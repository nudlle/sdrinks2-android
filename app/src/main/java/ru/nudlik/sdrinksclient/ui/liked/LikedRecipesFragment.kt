package ru.nudlik.sdrinksclient.ui.liked

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ru.nudlik.sdrinksclient.R

class LikedRecipesFragment : Fragment() {
    companion object {
        fun newInstance() = LikedRecipesFragment()
    }

    private lateinit var viewModel: LikedRecipesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.liked_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LikedRecipesViewModel::class.java)
        // TODO: Use the ViewModel
    }
}