package ru.nudlik.sdrinksclient.ui.recipes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.SDrinksApplication
import ru.nudlik.sdrinksclient.databinding.RecipesFragmentBinding

class RecipesFragment : Fragment() {
    companion object {
        fun newInstance() = RecipesFragment()
    }

    private lateinit var viewModel: RecipesViewModel
    private lateinit var dataBinding: RecipesFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.recipes_fragment, container, false)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dataBinding.recipeRecyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        viewModel = ViewModelProvider(this).get(RecipesViewModel::class.java)
        SDrinksApplication.getInstance().getAppComponent().inject(viewModel)
        viewModel.dataBinding = dataBinding
        viewModel.initialize()
    }
}
