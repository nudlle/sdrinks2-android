package ru.nudlik.sdrinksclient.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.SDrinksApplication
import ru.nudlik.sdrinksclient.databinding.AuthFragmentBinding

class AuthFragment : Fragment() {
    companion object {
        fun newInstance() = AuthFragment()
    }
    lateinit var dataBinding: AuthFragmentBinding
    lateinit var viewModel: AuthViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val contextWrapper = ContextThemeWrapper(activity, R.style.AppTheme_NoActionBar)
        val localInflater = inflater.cloneInContext(contextWrapper)
        dataBinding = DataBindingUtil.inflate(localInflater, R.layout.auth_fragment, container, false)
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        dataBinding.authViewModel = viewModel
        viewModel.authDataFragmentBinding = dataBinding
        SDrinksApplication.getInstance().getAppComponent().inject(viewModel)
        viewModel.initialize()
        viewModel.navController = Navigation.findNavController(view!!)
    }

    fun onBackPressed(): Boolean {
        return viewModel.handleBackPressed()
    }
}