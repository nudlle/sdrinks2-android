package ru.nudlik.sdrinksclient.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.ui.recipes.RecipesFragment
import ru.nudlik.sdrinksclient.ui.recipes.RecipesViewModel

class AboutFragment : Fragment() {
    companion object {
        fun newInstance() = RecipesFragment()
    }

    private lateinit var viewModel: RecipesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.recipes_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RecipesViewModel::class.java)
        // TODO: Use the ViewModel
    }
}