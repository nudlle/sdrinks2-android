package ru.nudlik.sdrinksclient.ui.own

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ru.nudlik.sdrinksclient.R

class OwnRecipesFragment : Fragment() {
    companion object {
        fun newInstance() = OwnRecipesFragment()
    }

    private lateinit var viewModel: OwnRecipesViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.own_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(OwnRecipesViewModel::class.java)
        // TODO: Use the ViewModel
    }
}