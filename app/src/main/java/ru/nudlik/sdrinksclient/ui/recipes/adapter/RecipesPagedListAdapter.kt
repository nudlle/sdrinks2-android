package ru.nudlik.sdrinksclient.ui.recipes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import ru.nudlik.sdrinksclient.R
import ru.nudlik.sdrinksclient.databinding.RvItemRecipeContentBinding
import ru.nudlik.sdrinksclient.listener.ItemClickListener
import ru.nudlik.sdrinksclient.model.recipeitem.RecipeItem

class RecipesPagedListAdapter constructor(diffUtilCallback: DiffUtil.ItemCallback<RecipeItem>) :
    PagedListAdapter<RecipeItem, RecipeItemViewHolder>(diffUtilCallback) {
    val itemClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RvItemRecipeContentBinding.inflate(inflater, parent, false)
        return RecipeItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecipeItemViewHolder, position: Int) {
        val recipeItem = getItem(position)
        holder.recipeItemDataBinding.recipeItem = recipeItem
        holder.recipeItemDataBinding.itemClicked = itemClickListener
        holder.recipeItemDataBinding.adapterPosition = position
        holder.bind(recipeItem)
        if (position == 0) {
            holder.replaceMargins()
        }
    }

}