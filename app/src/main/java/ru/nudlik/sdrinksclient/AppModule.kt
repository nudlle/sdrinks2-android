package ru.nudlik.sdrinksclient

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun getApplication(): Application {
        return application
    }
}