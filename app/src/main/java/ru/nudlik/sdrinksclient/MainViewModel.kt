package ru.nudlik.sdrinksclient

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import ru.nudlik.fragment.snackbar.SnackbarMaker
import ru.nudlik.sdrinksclient.databinding.MainActivityBinding
import ru.nudlik.sdrinksclient.extentions.gone
import ru.nudlik.sdrinksclient.extentions.visible
import ru.nudlik.sdrinksclient.model.database.repository.UserEntityRepository
import ru.nudlik.sdrinksclient.model.domain.User
import ru.nudlik.sdrinksclient.model.http.interceptors.BasicAuthInterceptor
import ru.nudlik.sdrinksclient.model.http.rest.ResponseCallback
import ru.nudlik.sdrinksclient.model.http.rest.UserRest
import ru.nudlik.sdrinksclient.model.inner.domain.UserEntity
import ru.nudlik.sdrinksclient.security.CryptoUtil
import ru.nudlik.sdrinksclient.security.CryptoUtilImpl
import ru.nudlik.sdrinksclient.security.SessionToken
import ru.nudlik.sdrinksclient.security.UserRequest
import ru.nudlik.sdrinksclient.ui.widget.CustomSearchView
import java.net.ConnectException
import java.net.SocketTimeoutException
import javax.inject.Inject

class MainViewModel : ViewModel() {
    lateinit var navController: NavController
//    lateinit var swipeToRefresh: SwipeRefreshLayout
    lateinit var searchView: CustomSearchView
    lateinit var dataBinding: MainActivityBinding
    @Inject
    lateinit var userRepository: UserEntityRepository
    @Inject
    lateinit var userRest: UserRest
    @Inject
    lateinit var basicAuthInterceptor: BasicAuthInterceptor
    private val cryptoUtil: CryptoUtil = CryptoUtilImpl()
    var userEntity: UserEntity? = null

    override fun onCleared() {
        super.onCleared()
    }

    fun onSearch(view: View) {
        if (searchView.query.isEmpty()) {
            searchView.setQuery("none", true)
        } else {
            searchView.setQuery(searchView.query, true)
        }
    }

    fun onCreateNewRecipe(view: View?) {
        if (userEntity?.user == null) {
            navController.navigate(R.id.action_recipesFragment_to_authFragment)
        } else {
            navController.navigate(R.id.action_recipesFragment_to_createRecipeFragment)
        }
    }

    fun onRefresh() {
        dataBinding.appBarMain.swipeRefresh.postDelayed({
            dataBinding.appBarMain.swipeRefresh.isRefreshing = false
        }, 2000)
    }

    fun onDestinationChange(controller: NavController, destination: NavDestination, arguments: Bundle?) {
        dataBinding.appBarMain.createNewRecipeFab.gone()
        when (destination.id) {
            R.id.authFragment -> {
                dataBinding.appBarMain.appBarMain.gone()
                dataBinding.appBarMain.swipeRefresh.isEnabled = false
            }
            R.id.recipesFragment -> {
                dataBinding.appBarMain.appBarMain.visible()
                dataBinding.appBarMain.swipeRefresh.isEnabled = true
                dataBinding.appBarMain.createNewRecipeFab.visible()
            }
        }
    }

    fun initialize(activity: MainActivity) {
        userRepository.getAllUserEntities().observe(activity, Observer {
            if (it.size == 1) {
                userEntity = it[0]
                it[0].user?.apply {
                    loginUser(this)
                }
            } else {
                basicAuthInterceptor.deleteCredintials()
            }
        })
    }
    private val loginCallback = object : ResponseCallback {
        override fun <T> success(data: T?) {
            val st = data as SessionToken
            basicAuthInterceptor.addCredentials(st)
        }

        override fun exception(e: Throwable?) {
            handleError(e)
        }
    }
    private fun handleError(e: Throwable?) {
        when (e) {
            is SocketTimeoutException ->
                SnackbarMaker.showLongSnackbar(dataBinding.appBarMain.parentContainerMain, R.string.timeout_error_title)
            is ConnectException -> {
                SnackbarMaker.showLongSnackbar(dataBinding.appBarMain.parentContainerMain, R.string.connect_error_title)
            }
            else -> {
                SnackbarMaker.showLongSnackbar(dataBinding.appBarMain.parentContainerMain, R.string.unknown_exception_title)
            }
        }
    }

    private fun loginUser(user: User) {
        val userLogin = User()
        userLogin.userId = user.userId
        userLogin.password = cryptoUtil.decode(CryptoUtil.PIN_ALIAS, user.password!!)
        val userRequest = UserRequest(userLogin, System.currentTimeMillis())
        userRest.loginUser(userRequest, loginCallback)
    }
}